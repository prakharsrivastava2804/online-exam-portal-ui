import { CONSTANTS } from './../../shared/constant/index';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../shared/services/index';

@Component({
  selector: CONSTANTS.MAIN.SELECTORS.LOGIN_COMPONENT,
  templateUrl: 'app/core/login/login-component.html',
  styleUrls: ['app/core/login/login-component.html']
})

export class LoginComponent implements OnInit {
  model: any = { username: '', password: '' };
  loading = false;
  returnUrl: string;
  registerLoginEmail: string;
  isRegisterPopupOpen: Boolean = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit() {
    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams[CONSTANTS.MAIN.STATIC_PROPERTY.RETUN_URL] || '/';
  }

  login() {
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(
      data => {
        this.router.navigate([this.returnUrl]);
      },
      error => {
        this.loading = false;
      });
  }

  openRegisterPopup() {
    this.isRegisterPopupOpen = true;
  }
  registerUser() {
    // logic to make api call for register
    console.log(this.registerLoginEmail);
  }
}
