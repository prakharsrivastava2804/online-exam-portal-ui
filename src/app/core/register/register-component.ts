import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { CONSTANTS } from './../../shared/constant/index';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../shared/services/index';

@Component({
  selector: CONSTANTS.MAIN.SELECTORS.REGISTER_COMPONENT,
  templateUrl: 'app/core/register/register-component.html',
  styleUrls: ['app/core/register/register-component.html']
})

export class RegisterComponent implements OnInit {
  userform: FormGroup;
  passwordform: FormGroup;
  isPasswordDisplay: Boolean = false;
  isUserFormDisplay: Boolean = true;
  collegeList: any[] = [
    { id: 1, name: 'A' },
    { id: 2, name: 'B' },
    { id: 3, name: 'C' },
    { id: 4, name: 'D' },
  ];
  token: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
  ) {
    this.token = this.route.snapshot.params[CONSTANTS.MAIN.STATIC_PROPERTY.TOKEN];

  }

  ngOnInit() {
    this.userform = this.fb.group({
      'name': new FormControl('', Validators.required),
      'college': new FormControl('', Validators.required)
    });

    this.passwordform = this.fb.group({
      'password': new FormControl('', Validators.compose([Validators.required, Validators.minLength(6)])),
      'confirmPassword': new FormControl('', Validators.compose([Validators.required, Validators.minLength(6)])),
    });

  }

  onPasswordSubmit(formData) {
    // api call to submit password
    console.log(formData);

  }

  onUserFormSubmit(formData) {
    // api call to submit password
    console.log(formData);
  }
}
