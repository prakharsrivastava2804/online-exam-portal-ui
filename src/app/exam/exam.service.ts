import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ExamService {
  QuestionUpdatedFromScreen$: Subject<any> = new Subject<any>();
  QuestionUpdatedFromNavPanel$: Subject<any> = new Subject<any>();
  constructor() {
    console.log('Exam Service Loaded');
  }

  updateQuestionfromScreen(questionNumber): void {
    this.QuestionUpdatedFromScreen$.next(questionNumber);
  }

  updateQuestionfromNavPanel(questionNumber): void {
    this.QuestionUpdatedFromNavPanel$.next(questionNumber);
  }

  clone(val: any) {
    try {
      return JSON.parse(JSON.stringify(val));
    } catch (ex) {
      return null;
    }
  }
}
