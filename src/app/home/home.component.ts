import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'as-home',
    templateUrl: 'home.html',
    styleUrls: [
        'home.css'
    ]
})
export class HomeComponent {
    imageSrc: any;
    imageList: any[] = [];
    constructor() {
        console.log('Home Component Loaded');
        this.imageList = [
            { 'src': 'https://raw.githubusercontent.com/christiannwamba/angular2-carousel-component/master/images/covered.jpg' },
            { 'src': 'https://raw.githubusercontent.com/christiannwamba/angular2-carousel-component/master/images/generation.jpg' },
            { 'src': 'https://raw.githubusercontent.com/christiannwamba/angular2-carousel-component/master/images/potter.jpg' },
            { 'src': 'https://raw.githubusercontent.com/christiannwamba/angular2-carousel-component/master/images/preschool.jpg' },
            { 'src': 'https://raw.githubusercontent.com/christiannwamba/angular2-carousel-component/master/images/soccer.jpg' }
        ];
    }
    onImageUploadFinished(event) {
        console.log(event);
        this.imageSrc = event.src;
    }
}
