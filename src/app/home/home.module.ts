import { ExamService } from './../exam/exam.service';
import { QuestionsComponent } from './../questions/questions-component';
import { CoreModule } from './../core/core-module';
import { NgModule } from '@angular/core';
import { SharedModule } from './../shared/shared.module';
import { HomeComponent } from './index';
import { QuestionsNavComponent } from '../questions-nav/index';
import { ExamManagementComponent } from '../exam/index';

@NgModule({
    imports: [
        SharedModule,
        CoreModule,
    ],
    declarations: [
        HomeComponent,
        ExamManagementComponent,
        QuestionsComponent,
        QuestionsNavComponent
    ],
    exports: [
        HomeComponent
    ],
    providers: [ExamService]
})
export class HomeModule {
}
