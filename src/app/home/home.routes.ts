import { Routes } from '@angular/router';
import { HomeComponent } from './index';
import { ExamManagementComponent } from '../exam/index';
import { RegisterComponent } from '../core/index';


export const HomeRoutes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'register/:token', component: RegisterComponent
  },
  {
    path: 'exams', component: ExamManagementComponent
  }

];
