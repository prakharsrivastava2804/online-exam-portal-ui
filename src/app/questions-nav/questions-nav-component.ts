import { ExamService } from './../exam/exam.service';
import { Component, OnInit } from '@angular/core';
import { CONSTANTS } from '../shared/index';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: CONSTANTS.MAIN.SELECTORS.QUESTION_NAV_COMPONENT,
  templateUrl: 'app/questions-nav/questions-nav-component.html',
  styleUrls: [
    'app/questions-nav/questions-nav-component.css'
  ]
})
export class QuestionsNavComponent implements OnInit {
  questionList: any[] = [];
  questionNumber: number;
  private componentSubscriptionsHandle: Subscription = new Subscription();

  constructor(private examService: ExamService) {
    this.questionNumber = 1;
    // subscribes to CartUpdated$ stream to update item

  }

  ngOnInit() {
    this.componentSubscriptionsHandle.add(
      this.examService
        .QuestionUpdatedFromScreen$
        .subscribe(questionNumber => {
          this.updateQuestionFromScreen(questionNumber);
        }));
    this.questionList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  }

  updateQuestionFromScreen(number: number) {
    this.questionNumber = number;
    console.log(number);
  }

  questionClicked(number) {
    this.questionNumber = number;
    this.examService.updateQuestionfromNavPanel(number);
    console.log(number);
  }
}
