import { Subscription } from 'rxjs/Subscription';
import { Component } from '@angular/core';
import { CONSTANTS } from '../shared/index';
import { ExamService } from '../exam/exam.service';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: CONSTANTS.MAIN.SELECTORS.QUESTION_COMPONENT,
  templateUrl: 'app/questions/questions-component.html',
  styleUrls: [
    'app/questions/questions-component.css'
  ]
})
export class QuestionsComponent implements OnInit {
  questionNumber: number;
  private componentSubscriptionsHandle: Subscription = new Subscription();
  constructor(private examService: ExamService) {
  }

  ngOnInit() {
    this.questionNumber = 1;
    this.componentSubscriptionsHandle.add(
      this.examService
        .QuestionUpdatedFromNavPanel$
        .subscribe(questionNumber => {
          this.updateQuestionFromPanel(questionNumber);
        }));

  }
  updateQuestionFromPanel(number) {
    // logic to make api call for the current question
    this.questionNumber = number;
    console.log(this.questionNumber);

  }

  updateQuestionFromScreen(isNext: boolean) {
    if (isNext !== undefined) {
      // logic to make api call for the current question
      this.questionNumber = isNext ? this.questionNumber + 1 : (this.questionNumber !== 1 ? this.questionNumber - 1 : 1);
    } else {
      // logic to submit the answer and fetch next question
    }
    this.examService.updateQuestionfromScreen(this.questionNumber);

    console.log(this.questionNumber);

  }

}
