export const MAIN = {
    APP: {
        BRAND: 'PCMS'
    },
    SELECTORS: {
        LOGIN_COMPONENT: 'login-component',
        QUESTION_COMPONENT: 'questions-component',
        QUESTION_NAV_COMPONENT: 'questions-nav-component',
        REGISTER_COMPONENT: 'register-component'
    },
    STATIC_PROPERTY: {
        MORE_INFO: 'moreInfo',
        ID: 'id',
        RETUN_URL: 'returnUrl',
        TOKEN: 'token'
    }
};
