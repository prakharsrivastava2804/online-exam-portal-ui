import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptionsArgs, RequestOptions, Response, ResponseOptions, ResponseType } from '@angular/http';

import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from './index';

@Injectable()
export class HttpClient {

  private refreshingAuthRequest: Promise<any>;

  constructor(
    private http: Http,
    private authService: AuthenticationService,

  ) { }

  request(options: RequestOptionsArgs): Observable<any> {
    let originalOptions = options;
    options = this.constructRequestOptions(options);
    this.createAuthorizationHeader(options.headers);
    return this.postIntercept(
      this.http.request(options.url, options)
    ).catch(error => {
      return <any>Observable.throw(error);
    });
  }


  private createAuthorizationHeader(headers: Headers) {
    let accessToken: string = this.authService.getAuthToken();
    if (accessToken) {
      headers.append('Authorization', 'Bearer ' + accessToken);
    }
  }

  private constructRequestOptions(options: RequestOptionsArgs) {
    options.headers = new Headers(options.headers || {});
    options = new RequestOptions(options);
    return options;
  }

  private postIntercept(observable: Observable<any>) {
    return observable.share();
  }
}
