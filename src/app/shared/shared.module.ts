import { AuthenticationService } from './services/auth-service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CarouselModule, MultiSelectModule, RatingModule, DropdownModule, PanelModule, DialogModule } from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    // ng prime components starts
    CarouselModule,
    DialogModule,
    DropdownModule,
    MultiSelectModule,
    PanelModule,
    RatingModule
    // ng prime components ends
  ],
  declarations: [
  ],
  exports: [
    CommonModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    // ng prime components starts
    CarouselModule,
    DialogModule,
    DropdownModule,
    MultiSelectModule,
    PanelModule,
    RatingModule,
    // ng prime components ends
  ],
  providers: [AuthenticationService]
})
export class SharedModule { }
